<?php

namespace App;

use  PDO;
class Student
{
    public $name;
    public $id;
    public function setData($data=''){

        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        return $this;

    }

    public function store(){
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=php38','root','');
            $query = "INSERT INTO 'students'('id','title') VALUES (:a, :b)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':b' => $this->name
                )
            );
        }catch (PDOException $e){
            echo 'Error: '. $e->getMessage();
        }
    }

    public function index(){
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=php38','root','');
            $query = "SELECT * FROM students";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        }catch (PDOException $e){
            echo 'Error: '. $e->getMessage();
        }
    }

    public function show(){
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=php38','root','');
            $query = "SELECT * FROM students WHERE id= $this->id ";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        }catch (PDOException $e){
            echo 'Error: '. $e->getMessage();
        }
    }
}